This will be the HOWTO for configuring nagios to monitor LMR equipment via SNMP.



# Files

cp -p submit_check_result /usr/lib64/nagios/plugins/eventhandlers/
cp -p sendAlertViaSendgrid.sh /usr/local/bin/
cp -pr snmp/* /etc/snmp/


# Testing

snmptrap -c public -v 2c 127.0.0.1 "" .1.3.6.1.4.1.21638.1.18.4.1

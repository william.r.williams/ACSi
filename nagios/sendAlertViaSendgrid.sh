#!/usr/bin/bash
####################################################################################################
#   sendAlertViaSendgrid.sh
#   Uses sendgrid restful api to forward email alerts from nagios
#   -Bill W. GBC/MA 21Oct2016
#   william.r.williams@gmail.com                         
####################################################################################################

# Variables from Nagios should be presented to this script in this order:
# $1 = $HOSTNAME$
# $2 = $HOSTSTATE$
# $3 = $HOSTOUTPUT$
# $4 = $LONGDATETIME$
# $5 = $NOTIFICATIONTYPE$
# $6 = $HOSTADDRESS$
# $7 = $CONTACTEMAIL$

# set up static variables here
APIKEY=""
TXR="nagiosalerts@nagiostest1"
RXR="${7}"
SBJ="SNMP Alert for ${1}"
#MSG="$(/usr/bin/printf "%b" "\*\*\*\*\* Nagios \*\*\*\*\*\n\nNotification Type: ${5}\nHost: ${1}\nState: ${2}\nAddress: ${6}\nInfo: ${3}\n\nDate/Time: ${4}\n")"
MSG="++++NAGIOS SNMP ALERT++++<br>Notification Type: ${5}<br>Host: ${1}<br>State: ${2}<br>Address: ${6}<br>Info: ${3}<br>Date\/Time: ${4}"

# get args from Nagios alert

# create temp file
CTMP=$(mktemp)
cat <<'CTMP' > $CTMP
curl  --proxy https://llproxy.llan.ll.mit.edu:8080 \
  --request POST \
  --url https://api.sendgrid.com/v3/mail/send \
  --header 'Authorization: Bearer APIKEY' \
  --header 'Content-Type: application/json' \
  --data '{"personalizations": [{"to": [{"email": "RXR"}]}],"from": {"email": "TXR"},"subject": "SBJ","content": [{"type": "text/html", "value": "MSG"}]}'
CTMP

sed -e "s/APIKEY/$APIKEY/" -e "s/TXR/$TXR/" -e "s/RXR/$RXR/" -e "s/SBJ/$SBJ/" -e "s/MSG/$MSG/" -i $CTMP

chmod u+x $CTMP

# execute temp script to send alert then delete it

$CTMP

rm -f $CTMP

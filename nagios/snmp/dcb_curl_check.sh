#!/bin/bash
####################################################################################################
#   dcb_curl_check.sh
#   Nagios check for DCB VPN Devices using curl and comparing exit codes.
#   -Bill W. GBC/MA 29Nov2016
#   william.r.williams@gmail.com                         
####################################################################################################

DCBWEB=205.166.54.172

# The below curl command taken from DCB's snmp documentation 'CURL.txt'

DCBOUT="$(curl -gk --user admin: https://${DCBWEB}/cgi-bin/menuform.cgi -F \
select=Status_Menu -F form=status_tunnel_nodes)"


LAN1STATUS="$(grep lan1 <<<${DCBOUT} | awk '{print $6}')"

if [ "${LAN1STATUS}" = "up" ]; then
  echo "Interface OK"
  exit 0
else
  echo "Interface Down"
  exit 1
fi

# Getting the Tunnel Nodes status page:
# https://205.166.54.172/cgi-bin/menuform.cgi?select=Status_Menu&form=status_tunnel_nodes
